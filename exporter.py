from helpers import fs, sql
#from pbar import PBar


runID = int(input("Which run to export?: "))
gen_count = sql.autoread("gatar_runs", "run" + str(runID), select="generation")[-1] + 1


#pb = PBar(label="  Getting data from SQL  ")

meta = sql.autoread("gatar_meta", "meta", extra="WHERE id = " + str(runID))[0]

generations = []

for gID in range(gen_count):
    print("Generation: " + str(gID))
    drivers = []
    for driver in sql.autoread("gatar_runs", "run" + str(runID), extra="WHERE generation = " + str(gID)):
        print(driver)
        prep = {"points": driver["points"]}
        tele = []
        for telemetry in sql.autoread("gatar_telemetry", "run" + str(runID), extra="WHERE id = " + str(driver["id"])):
            tele.append(
                {
                    "x": telemetry["x"],
                    "y": telemetry["y"],
                    "angle": telemetry["angle"],
                    "gas": telemetry["gas"],
                    "brake": telemetry["brake"],
                    "steeringwheel_angle": telemetry["steeringwheel_angle"]
                }
                )

        prep["telemetry"] = tele
        drivers.append(prep)
        
    generations.append(drivers)
    
#pb.end()
    
print("Writing data to file: run" + str(runID) + ".json")

data = {
        "meta": meta,
        "data": generations
    }

fs.saveJson("run" + str(runID) + ".json", data)
