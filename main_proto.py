from scanner import Scanner
from scanner2 import Scanner as sc
from scannerX import ScannerX as sx
#from car import Car
from driver import Driver
from helpers import intersect, fs
from math import pi
from math import atan
from matplotlib import pyplot as plt
from neuralNetwork.NeuralNetwork import NeuralNetwork as NN
from weather import Weather
from car_stuff.Car import Car, Large_Sedan
#import carParts


weatherstate = Weather("snowy") #Beskriver vilket väder som ska simuleras, "snowy"
generationSize = 10    
generationLength = 1
generationAmount = 1
track = fs.loadJson("tracks/mantorp.json")
carDeathEnabled = True
carmodel = Large_Sedan
angs = [
    pi/2+0.000001, # ang0
    pi/2+1.000001, # ang1
    pi/2+2.000001, # ang2
    pi/2+3.000001, # ang3
    pi/2+4.000001, # ang4
    pi/2-1.000001, # ang5
    pi/2-2.000001, # ang6
    pi/2-3.000001, # ang7
    pi/2-4.000001  # ang8
    ]
max_scanner_distance = 1000



#state = [weatherstate, carchassi, carmotor]
#print("Weather: " + str(weather) + " Car chassi: " + str(carchassi) + " Car motor: " + str(carmotor))


#car = Car(x = 800, y = 580, ang = 0.0) # track["start"][0] + 1, track["start"][1] + 2.5

car = Large_Sedan()
car.x = track["start"][0]
car.y = track["start"][1]

#scanner1 = Scanner(ang = pi/2+0.000001, dist = 50, car = car, track = track, debug = False)
#scanner2 = Scanner(ang = pi/2+1.000001, dist = 50, car = car, track = track, debug = False)
#scanner3 = Scanner(ang = pi/2+2.000001, dist = 50, car = car, track = track, debug = False)
#scanner4 = Scanner(ang = pi/2+3.000001, dist = 50, car = car, track = track, debug = False)
#scanner5 = Scanner(ang = pi/2+4.000001, dist = 50, car = car, track = track, debug = False)
#scanner6 = Scanner(ang = pi/2+5.000001, dist = 50, car = car, track = track, debug = False)
#scannerX = sx(angs = [pi/2+0.000001, pi/2+1.000001, pi/2+2.000001, pi/2+3.000001, pi/2+4.000001, pi/2+0.500001, pi/2+0.500001, pi/2+0.500001, pi/2+0.500001, pi/2+0.500001, pi/2+0.500001, pi/2+0.500001], dist = 50, car = car, track = track, debug = True)


from time import time

start = time()

#for _ in range(1):
    #print(scanner1.scan())
    #print(scanner2.scan())
    #print(scanner3.scan())
    #print(scanner4.scan())
    #print(scanner5.scan())
    #print(scanner6.scan())
    #print(scanner1.scan())
    #print(scanner2.scan())
    #print(scanner3.scan())
    #print(scanner4.scan())
    #print(scanner5.scan())
    #print(scanner6.scan())

#print(time() - start)

#start = time()

#for i in range(1):
    #print(scannerX.scan())

#print(time() - start)

#while True:
    #scannerX.scan()
    #car.x += float(input("x delta: "))
    #car.y += float(input("y delta: "))

xs2 = []
ys2 = []

xs2.append(track["outer"][-1][0])
ys2.append(track["outer"][-1][1])

for i in range(len(track["outer"])):
    xs2.append(track["outer"][i][0])
    ys2.append(track["outer"][i][1])


plt.plot(xs2, ys2, marker = 'o')


xs3 = []
ys3 = []

xs3.append(track["inner"][-1][0])
ys3.append(track["inner"][-1][1])

for i in range(len(track["inner"])):
    xs3.append(track["inner"][i][0])
    ys3.append(track["inner"][i][1])

plt.plot(xs3, ys3, marker = 'o')


k = (track["heading"][1]-track["start"][1])/(track["heading"][0]-track["start"][0])
trackStartingdirection = 512
if (track["heading"][1]-track["start"][1]) < 0:
    if (track["heading"][0]-track["start"][0]) == 0:
        trackStartingdirection = pi/2
    else:
        trackStartingdirection = atan(k)
elif (track["heading"][1]-track["start"][1]) > 0:
    if (track["heading"][0]-track["start"][0]) == 0:
        trackStartingdirection = 3*pi/2
    else:
        trackStartingdirection = atan(k)+pi
        
        # from blah import Large_seda
        # model = largesedan


neuralNetworkList = [NN((2 * len(angs)) + 3, 14, 11, 3) for _ in range(GenerationSize)]
driverList = [Driver(id, neuralNetworkList[id], track, track["start"][0], track["start"][1], trackStartingdirection, carmodel, max_scanner_distance, angs) for id in range(len(neuralNetworkList))]
DebugList = []
finishedDrivers = []
percentFinished = 0
for i in range(GenerationLength):
    if i % (GenerationLength/100) == 0:
        percentFinished+= 1
        print(str(percentFinished) + "%")
    deadDrivers = []
    deadDriver = False
    for driver in driverList:
        driver.drive()
        #if driver.ded == True and CarDeathEnabled:
        #    deadDrivers.append(driver)
        #    deadDriver = True
    if deadDriver == True:
        for i in range(len(deadDrivers)):
            driverList.remove(deadDrivers[i])
            finishedDrivers.append(deadDrivers[i])
    plt.plot(driver.car.x, driver.car.y, marker = "o")
print("Generation complete!")
for driver in driverList:
    print("Driver id: " + str(driver.id) + " points: " + str(driver.points))
    
#for i in range(len(DebugList)):
    #print(DebugList[i])
for driver in driverList:
    plt.plot(driver.car.x, driver.car.y, marker = "o")

#Drivertest.car.v = 0
#Drivertest.car.updPos()

#plt.plot(Drivertest.car.x, Drivertest.car.y, marker = "o")

#Drivertest.car.steeringTarget = pi/8
#Drivertest.car.throttle = 1.0

#for _ in range(10**3):
    #print(Drivertest.car.steering, Drivertest.car.ang)
    #Drivertest.car.updSpd()
    #Drivertest.car.updAng()
    #Drivertest.car.updPos()
    #plt.plot(Drivertest.car.x, Drivertest.car.y, marker = "o")
    #print(Drivertest.car.v*3.6)
    
#print(Drivertest.car.ang)
#Drivertest.car.updSpd()
#Drivertest.car.updAng()
#Drivertest.car.updPos()
#plt.plot(Drivertest.car.x, Drivertest.car.y, marker = "o")
#print(Drivertest.car.ang)
    
#Drivertest.car.steeringTarget = -pi/4
#Drivertest.car.throttle = 1.0

#for _ in range(10**3):
    #print(Drivertest.car.steering, Drivertest.car.ang)
    #Drivertest.car.updSpd()
    #Drivertest.car.updAng()
    #Drivertest.car.updPos()
    #plt.plot(Drivertest.car.x, Drivertest.car.y, marker = "o")


#Drivertest.car.steeringTarget = 0

#Drivertest.car.throttle = 0.0
#Drivertest.car.brake = 1.0

#for _ in range(10):
#    Drivertest.car.updSpd()
#    Drivertest.car.updAng()
#    Drivertest.car.updPos()
#    plt.plot(Drivertest.car.x, Drivertest.car.y, marker = "o")
#    print(Drivertest.car.v*3.6)
        
                            

plt.show()
