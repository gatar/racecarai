from scannerX import ScannerX, NoNodesException


class Driver:
    def __init__(self, NN, track, ang, car_model, dist, angs, life): 
        self.NN = NN
        self.track = track
        self.track_length = track["inner"][-1][3]
        self.car = car_model()
        self.car.life = life
        self.car.x = track["start"][0]
        self.car.y = track["start"][1]
        self.car.car_angle.setAngle(ang)
        self.points = 0
        self.laps = 0
        self.previous_distance_from_start = 0
        self.scanner = ScannerX(angs = angs, dist = dist, car = self.car, track = track, debug = True)
        self.dead = False
        self.dead = not self.drive()
        if self.car.v.magnitude == 0:
            self.dead = True
        self.car.x = track["start"][0]
        self.car.y = track["start"][1]
        self.car.car_angle.setAngle(ang)
        self.car.v.setMA(0, ang)
        self.points = 0
        self.laps = 0
        
    # comparators
    def __lt__(self, other): # <
        return self.points < other.points
    
    def __le__(self, other): # <=
        return self.points <= other.points
    
    def __eq__(self, other): # ==
        return self.points == other.points
    
    def __ne__(self, other): # !=
        return self.points != other.points
    
    def __ge__(self, other): # >=
        return self.points >= other.points
    
    def __gt__(self, other): # >
        return self.points > other.points
    
            
    def drive(self):
        
        inputs = []
        
        try:
            scan = self.scanner.scan()
        except NoNodesException:
            self.dead = True
            return False
        
        distance_from_start = scan[0]
        distances_from_sides = scan[1:]
        
        if distance_from_start - self.previous_distance_from_start < -self.track_length - 100: #innan var det här delat med två istället för minus 100
            self.laps += 1
        elif distance_from_start - self.previous_distance_from_start > self.track_length - 100:
            self.laps -= 1

        self.points = distance_from_start + (self.laps * self.track_length)
        
        isinner = False
        isouter = False
        
        for i in range(int((self.NN.input_nodes-1)/2)):
                inputs.append(distances_from_sides[i][0])
                if distances_from_sides[i][0] <= self.car.chassis.car_width/2:
                    self.dead = True
                    return False

                if distances_from_sides[i][1] == "inner":
                    inputs.append(1)
                    isinner = True
                elif distances_from_sides[i][1] == "outer":
                    inputs.append(0)
                    isouter = True
                else:
                    inputs.append(0.5)
        if not (isinner and isouter):
            return False
        inputs.append(int(self.car.v.magnitude))

        if self.dead:
            return False
            
        outputs = self.NN.predict(inputs)
        self.car.steer(outputs)
        self.previous_distance_from_start = distance_from_start
        return True
