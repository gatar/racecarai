import tensorflow as tf
from helpers import fs
from car_stuff.Car import Large_Sedan
from math import pi, atan, atan2, log10
from driver import Driver
from neuralNetwork.NeuralNetwork import NeuralNetwork as NN
from helpers import sql, fs
from multiprocessing import cpu_count
from time import time
from matplotlib import pyplot as plt
from os import makedirs
#tf.debugging.set_log_device_placement(True)


# debug options

show_plot = False
save_plot = False
print_driving = True
print_task = True


# setup data
track = fs.loadJson("tracks/mantorp.json")
car_model = Large_Sedan

angs = [
    0.000001,
    -0.030001,
    0.030001,
    -0.080001,
    0.080001,
    -0.150001,
    0.150001,
    -0.320001,
    0.320001,
    -0.900001,
    0.900001
    ]

life_time = 1000
max_scanner_distance = 40

sample_size = 3
keep_ratio = 0.3
original_mutation_rate = 0.5
mutation_rate = original_mutation_rate
generations = 1000

#Struktur för neurala nätverk
input_shape = (2 * len(angs)) + 1
hidden1_shape = 20
hidden2_shape = 16
hidden3_shape = 13
output_shape = 3

keep_count = max(int(sample_size * keep_ratio), 1)

# calculate car starting direction
start_direction = pi#atan2((track["heading"][1]-track["start"][1]),(track["heading"][0]-track["start"][0]))
    
def load_NN(path):
    new_model = tf.keras.models.load_model(path)
    return NN(
        input_shape,
        hidden1_shape,
        hidden2_shape,
        hidden3_shape,
        output_shape,
        model = new_model
)

def generate_drivers(target):
    
    new_drivers = []
    
    while len(new_drivers) < target:
        neuro = NN(input_shape, hidden1_shape, hidden2_shape, hidden3_shape, output_shape)
        neuro.mutate(1)
        try_driver = Driver(neuro, track, start_direction, car_model, max_scanner_distance, angs, life_time)
        
        if not try_driver.dead:
            new_drivers.append(try_driver)
            
    return new_drivers

# give cars to drivers
def seat_drivers(NNs):
    
    new_drivers = []
    
    for NN in NNs:
        new_drivers.append(Driver(NN, track, start_direction, car_model, max_scanner_distance, angs, life_time))
            
    return new_drivers

def run_driver(driver):
    if(print_driving):
        print("Driving:")
    while not driver.dead:
        driver.drive()
        #if(show_plot or save_plot):
            #plt.plot(driver.car.x, driver.car.y, marker = "o")
        if(print_driving):
            print("-", end = '')
    #if(show_plot or save_plot):
        #plt.plot(driver.car.x, driver.car.y, marker = "x")
    if(print_driving):
        print("|")
        print("Points: " + str(driver.points))
    return driver

# create initial drivers
drivers = generate_drivers(sample_size)

# setup SQL
if not fs.exists("configs/sql.json"):

    sqlcfg = {

        "host": "localhost",
        "user": "root",
        "password": ""

        }

    fs.saveJson("configs/sql.json", sqlcfg)

# setup sql databases
dbRoot, dbRootCursor = sql.connect()

dbRootCursor.execute("SHOW DATABASES")
dbs = [x[0] for x in dbRootCursor]
if "gatar_meta" not in dbs: dbRootCursor.execute("CREATE DATABASE gatar_meta")
if "gatar_runs" not in dbs: dbRootCursor.execute("CREATE DATABASE gatar_runs")
if "gatar_telemetry" not in dbs: dbRootCursor.execute("CREATE DATABASE gatar_telemetry")


dbMeta, dbMetaCursor = sql.connect("gatar_meta")

dbMetaCursor.execute("SHOW TABLES")
tbls = [x[0] for x in dbMetaCursor]

if "meta" not in tbls: dbMetaCursor.execute(
    "CREATE TABLE meta ("
    "id INT AUTO_INCREMENT PRIMARY KEY,"
    "track VARCHAR(64),"
    "generations INT(8),"
    "sample_size INT(8),"
    "mutation_rate DECIMAL(18, 7),"
    "keep_ratio DECIMAL(18, 7),"
    "keep_count INT(8)"
    ")"
    )

dbRoot.close()
dbRootCursor.close()
dbMeta.close()
dbMetaCursor.close()


dbRuns, dbRunsCursor = sql.connect("gatar_runs")
dbTelemetry, dbTelemetryCursor = sql.connect("gatar_telemetry")

sql.autoinsert("gatar_meta", "meta", {"track": track["name"] + ".png", "generations": generations, "sample_size": sample_size, "mutation_rate": mutation_rate, "keep_ratio": keep_ratio, "keep_count": keep_count})
runID = sql.lastrow("gatar_meta", "meta")

print("Run: " + str(runID))

dbRunsCursor.execute(
    "CREATE TABLE run" + str(runID) + " ("
    "id INT AUTO_INCREMENT PRIMARY KEY,"
    "generation INT(8),"
    "points INT(8)"
    ")"
    )

dbTelemetryCursor.execute(
    "CREATE TABLE run" + str(runID) + " ("
    "id INT(8),"
    "x DECIMAL(18, 7),"
    "y DECIMAL(18, 7),"
    "angle DECIMAL(18, 7),"
    "gas DECIMAL(18, 7),"
    "brake DECIMAL(18, 7),"
    "steeringwheel_angle DECIMAL(18, 7)"
    ")"
    )

dbRuns.close()
dbRunsCursor.close()

dbTelemetry.close()
dbTelemetryCursor.close()

teleID = 1

makedirs("saves/run" + str(runID))

# main loop
for generation in range(generations):
    print("Running generation " + str(generation) + "...")
    start = time()

    if(show_plot or save_plot):
        xs2 = []
        ys2 = []

        xs2.append(track["outer"][-1][0])
        ys2.append(track["outer"][-1][1])

        for i in range(len(track["outer"])):
            xs2.append(track["outer"][i][0])
            ys2.append(track["outer"][i][1])


        plt.plot(xs2, ys2, marker = 'o')


        xs3 = []
        ys3 = []

        xs3.append(track["inner"][-1][0])
        ys3.append(track["inner"][-1][1])

        for i in range(len(track["inner"])):
            xs3.append(track["inner"][i][0])
            ys3.append(track["inner"][i][1])

        plt.plot(xs3, ys3, marker = 'o')
        if(print_task):
            print("Track plotted")
    
    #try:
    # run drivers
    result = list(map(run_driver, drivers))
    if(print_task):
        print("Drivers have been run")
    result.sort(reverse=True)

    mutation_rate = original_mutation_rate/(log10(result[0].points + generation))
    print("Mutation_rate: " + str(mutation_rate))
        
    datalist = []
    telelist = []
    
    # save result
    for res in result:
        datalist.append({"generation": generation, "points": res.points})
        
        xs = []
        for x in res.car.telemetry.x:
            xs.append(float(x))
            
        ys = []
        for y in res.car.telemetry.y:
            ys.append(float(y))
            
        cangs = []
        for cang in res.car.telemetry.angle:
            cangs.append(float(cang))
            
        gass = []
        for gas in res.car.telemetry.gas:
            gass.append(float(gas))
            
        brakes = []
        for brake in res.car.telemetry.brake:
            brakes.append(float(brake))
            
        steeringwheel_angles = []
        for steeringwheel_angle in res.car.telemetry.steeringwheel_angle:
            steeringwheel_angles.append(float(steeringwheel_angle))
            
        for i in range(len(xs)):
            telelist.append({"id": teleID, "x": xs[i], "y": ys[i], "angle": cangs[i], "gas": gass[i], "brake": brakes[i], "steeringwheel_angle": steeringwheel_angles[i]})
        
        teleID += 1
        
    sql.multinsert("gatar_runs", "run" + str(runID), datalist)
    sql.multinsert("gatar_telemetry", "run" + str(runID), telelist)

    if(print_task):
        print("Results saved")
    
    # extract best NNs
    kept = []

    for i in range(keep_count):
        kept.append(result[i].NN)
        
    # refill with mutated NNs
    NNs = []
    NNs = kept
    
    while len(NNs) < sample_size:
        #nextNNs = NNs[:keep_count].copy()
        nextNNs = []
        for NN in kept:
            nextNN = NN.copy()
            nextNN.mutate(mutation_rate)
            nextNNs.append(nextNN)
        
        NNs += nextNNs
    
    # delete overflow
    NNs = NNs[:sample_size]

    # create new drivers
    drivers = seat_drivers(NNs)
    if(print_task):
        print("Drivers seated")

    #except KeyboardInterrupt:
    #    break
    print("DONE! Time for generation " + str(generation) + " was: " + str(time()-start) + " seconds. That was on average " + str((time()-start)/sample_size) + " seconds per driver")
    if(save_plot or show_plot):
        if(show_plot):
            if(print_task):
                print("Showing plot")
            plt.show()
        if(save_plot):
            if(print_task):
                print("Saving plot")
            plt.savefig("saves/run" + str(runID) + "/gen" + str(generation))
            if(print_task):
                print("Saved plot")
        plt.clf()
        if(print_task):
            print("Cleared plot")
    
