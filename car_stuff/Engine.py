# torque, total torque at wheels of axle (1/2 on each wheel of axle), Nm


class Engine:
    
    
    def __init__(self, front_torque, rear_torque):
        self.front_torque = front_torque
        self.rear_torque = rear_torque
        
        
class Stronk_Boi_9K(Engine):
    
    
    def __init__(self):
        super().__init__(200*10**3, 300*10**3)
