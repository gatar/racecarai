# torque, total torque at wheels of axle (1/2 on each wheel of axle), Nm

from scipy.interpolate import interp1d


class Drivetrain:
    
    
    def __init__(self, power, max_torque):
        self.power = power
        self.max_torque = max_torque
        
        
    def get_torque(self, rads, throttle):
        
        if rads == 0:
            rads = 0.00001
            
        return min(((self.power / rads) * throttle), self.max_torque)
        
        
class Stronk_Boi_9K(Drivetrain):
    
    
    def __init__(self):
        super().__init__(95000, 1300)
        
        
        
#import matplotlib.pyplot as plt

#eng = Stronk_Boi_9K()

#x = [x for x in range(0, 160, 1)]
#y = []

#for xi in x:
#    y.append(eng.get_torque((xi/3.6)/0.25, 1.0))
#    
#plt.plot(0, 0, marker="o")
#plt.plot(x, y, marker="o")
#plt.show()
