class Wheel:
    
    
    def __init__(self, mass, radius, friction):
        self.mass = mass # kg
        self.radius = radius # m
        self.friction = friction
        self.angle = 0 # Detta är relativt till bilens vinkel
        self.down_force = 1000
        
class Grippy19(Wheel):
    
    
    def __init__(self):
        super().__init__(20, 0.2413 + 0.10, 0.95)
