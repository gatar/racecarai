from Vector import Vector2D as Vector
from math import pi, tan, cos, sin, atan, atan2

class Physics:
    
    
    def __init__(self):
        self.weather = "default"
        self.f_res = Vector()
    
    def fsx_update_long_force(self):
        max_brake_force = self.fr_wheel.down_force + self.fl_wheel.down_force + self.rr_wheel.down_force + self.rl_wheel.down_force
        max_gas_force = self.rr_wheel.down_force + self.rl_wheel.down_force
        alpha = self.v.angle - self.car_angle.angle
        b = (self.chassis.drag_coefficient * 1.2 * (self.chassis.car_height * ((abs(cos(alpha)) * self.chassis.car_width) + (abs(sin(alpha)) * self.wheelbase)))) / 2
        r_wheel_force = max(-max_brake_force, min(max_gas_force, (self.drivetrain.get_torque(self.v.split(self.car_angle.angle).x/self.rr_wheel.radius, self.gas)/self.rr_wheel.radius) - (self.brake * 100000)))
        f_traction = Vector(magnitude = r_wheel_force, angle = self.car_angle.angle)
        f_rr = self.calc_rolling_resistance(alpha, b)
        f_drag = self.calc_air_drag(alpha, b)
        self.f_res = f_traction + f_rr + f_drag
        self.a = self.f_res/self.mass
        
        
    def fsx_update_lowspeed(self):
        self.v += (self.a) * self.dt
        velocity = self.v.magnitude
        acceleration = self.a.split(self.car_angle.angle).x
        vel_angle = self.car_angle.angle
        dxy = Vector()
        if (self.fl_wheel.angle > 0.01) or (self.fl_wheel.angle < -0.01):
            radius = -self.wheelbase / tan(self.fl_wheel.angle)
            distance = velocity * self.dt + (acceleration * self.dt * self.dt / 2)
            dangle = distance / radius
            self.car_angle.angle += dangle
            dy = (radius * (1 - cos(dangle))) + ((self.wheelbase / 2) * sin(dangle))
            dx = (radius * sin(dangle)) - ((self.wheelbase / 2) * (1 - cos(dangle)))
            dxy = Vector(x = dx, y = dy)
            dxy.setAngle(dxy.angle + self.car_angle.angle)
            self.v.setMA(velocity, vel_angle)
            self.x += dxy.x
            self.y += dxy.y
        else:
            self.v.setMA(velocity, self.car_angle.angle)
            self.x += self.v.x * self.dt + (self.a.x * self.dt * self.dt / 2)
            self.y += self.v.y * self.dt + (self.a.y * self.dt * self.dt / 2)
        self.v.setMA(self.v.split(self.car_angle.angle).x, self.car_angle.angle)
    
    def calc_rolling_resistance(self, alpha, b): #Approximering för rullmotstånd
        return Vector(magnitude = self.v.split(self.car_angle.angle).x * 30 * -b, angle = self.car_angle.angle)
        
    def calc_air_drag(self, alpha, b):
        return self.v * -b * self.v.magnitude