# brakes, [front_left, front_right, rear_left, rear_right]
# torques, same scheme as brakes but each index contains [accel_torque, deccel_torque], Nm

from car_stuff import Brake, Chassis, Drivetrain, Wheel
from car_stuff.Physics import Physics
from math import sqrt

from Vector import Vector2D as Vector


class Telemetry:
    
    def __init__(self, car):
        self.car = car
        
        self.x = []
        self.y = []
        self.angle = []
        self.gas = []
        self.brake = []
        self.steeringwheel_angle = []
        
    def track(self):
        
        self.x.append(self.car.x)
        self.y.append(self.car.y)
        self.angle.append(self.car.car_angle.angle)
        self.gas.append(self.car.gas)
        self.brake.append(self.car.brake)
        self.steeringwheel_angle.append(self.car.steeringwheel_angle)
        


class Car(Physics):
    
    
    def __init__(self, chassis, drivetrain, wheels, brakes):
        super().__init__() # get stuff from Physics
        
        self.telemetry = Telemetry(self)
        
        self.tick_distance = 0.5 # distance between each update in metres
        self.dt = 1
        self.max_dt = 2
        self.min_dt = 0.01
        
        self.life = 1337
        
        self.v = Vector()
        self.a = Vector()
        self.x = 0
        self.y = 0
        
        self.pathX = []
        self.pathY = []

        self.car_angle = Vector(angle = 0, magnitude = 1)
        
        self.wheelbase = chassis.wheelbase
        self.com = chassis.com
        self.mass = chassis.mass
        self.drivetrain = drivetrain
        self.chassis = chassis
        
        self.steeringwheel_angle = 0
        self.gas = 0
        self.brake = 0
        
        for w in wheels:
            w.down_force = self.mass/4
        
        self.fl_wheel = wheels[0]
        self.fr_wheel = wheels[1]
        self.rl_wheel = wheels[2]
        self.rr_wheel = wheels[3]
        
        self.fl_brake = brakes[0]
        self.fr_brake = brakes[1]
        self.rl_brake = brakes[2]
        self.rr_brake = brakes[3]
        
        
    def updPos(self):
        # check for dead
        #print("1 Pos x: " + str(self.x) + " y: " + str(self.y))
        if self.life <= 0.0:
            self.dead = True
            print("I died from lack of time ;-;")
            
        self.v.updMA()
        
        self.fsx_update_long_force()
        
        if (self.a.magnitude == 0):
            self.dt = max(min(self.max_dt, self.tick_distance/max(self.v.magnitude, 0.00000000001)), self.min_dt)
        elif (self.a.magnitude > 0):
            self.dt = max(min(self.max_dt, sqrt(((self.v.magnitude * self.v.magnitude) / (0.000001 + self.a.magnitude * self.a.magnitude)) + (2 * self.tick_distance / (0.0000001 + self.a.magnitude))) - (self.v.magnitude / (0.0000001 + self.a.magnitude))), self.min_dt)
        else:
            self.dt = 1
            
        if self.dt > self.life:
            self.dt = self.life
            
        self.life -= self.dt
        
        self.fsx_update_lowspeed()
        #print("dt = " + str(self.dt) + " v = " + str(self.v.magnitude))
        #print("2 Pos x: " + str(self.x) + " y: " + str(self.y))
            
            
    def updSteering(self, target):
        if self.steeringwheel_angle < target:
            self.steeringwheel_angle += (self.chassis.steering_speed * self.dt)
        elif self.steeringwheel_angle > target:
            self.steeringwheel_angle -= (self.chassis.steering_speed * self.dt)
            
        if self.steeringwheel_angle < self.chassis.min_ang:
            self.steeringwheel_angle = self.chassis.min_ang
        elif self.steeringwheel_angle > self.chassis.max_ang:
            self.steeringwheel_angle = self.chassis.max_ang
        self.fr_wheel.angle = self.steeringwheel_angle
        self.fl_wheel.angle = self.fr_wheel.angle
            
    def steer(self, inputs):
        target = 2 * (inputs[0] - 0.5)
        self.gas = inputs[1]
        self.brake = inputs[2]
        self.updSteering(target)
        self.updPos()
        self.telemetry.track()


class Large_Sedan(Car):
    
    
    def __init__(self):
        super().__init__(
            Chassis.Large_Sedan(), 
            Drivetrain.Stronk_Boi_9K(), 
            [Wheel.Grippy19(), Wheel.Grippy19(), Wheel.Grippy19(), Wheel.Grippy19()], 
            [Brake.A_Brake(), Brake.A_Brake(), Brake.A_Brake(), Brake.A_Brake()]
            )
