# com stands for Centre of Mass
# supplied as [x, y, z]
# x = 0 when inline left-right with center of car, negative is left
# y = 0 when inline front-back with center of wheelbase, negative is back
# z = 0 when com is on the ground, top-bottom, can only be positive

# wheelbase, length of wheelbase, m
# com, list for position of com, [x, y, z], m
# mass, mass, kg


class Chassis: # parent class
    
    
    def __init__(self, wheelbase, com, mass, steering_speed, min_ang, max_ang, drag_coefficient, car_height, car_width):
        self.wheelbase = wheelbase
        self.com = com
        self.mass = mass
        self.car_height = car_height
        self.car_width = car_width
        self.steering_speed = steering_speed # rad/s
        self.min_ang = min_ang
        self.max_ang = max_ang
        self.drag_coefficient = drag_coefficient
        

class Large_Sedan(Chassis): # child class of Chassis, based on BMW E60
    
    
    def __init__(self): # super() accesses parent class methods
        super().__init__(2.89, [0, 0, 0.7], 1700, 1.0, -0.9, 0.9, 0.32, 1.5, 1.85)
        
