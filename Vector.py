from math import sin, cos, atan, sqrt, pi


class Vector2D:
    
    
    def __init__(self, *, angle = None, magnitude = None, x = None, y = None):
    
        self.angle = 0.0
        self.magnitude = 0.0
        self.x = 0.0
        self.y = 0.0
        
        if (angle is not None) and (magnitude is not None) and (x is None) and (y is None):
        
            self.angle = angle
            self.magnitude = magnitude
            
            self.updXY()
            
        elif (x is not None) and (y is not None) and (angle is None) and (magnitude is None):
            
            self.x = x
            self.y = y
            
            self.updMA()
            
        elif (x is None) and (y is None) and (angle is None) and (magnitude is None):
            
            self.x = 0
            self.y = 0
            
            self.updMA()
            
        else:
            raise Exception("Requires angle & magnitude or x & y or empty")
        
    
    def __repr__(self):
        return "angle: " + str(self.angle) + ", magnitude: " + str(self.magnitude) + ", x: " + str(self.x) + ", y: " + str(self.y)
    
    
    def __str__(self):
        return "angle: " + str(self.angle) + ", magnitude: " + str(self.magnitude) + ", x: " + str(self.x) + ", y: " + str(self.y)
    
    
    def __getitem__(self, key):
        return {"angle": self.angle, "magnitude": self.magnitude, "x": self.x, "y": self.y}[key]
    
    
    def __add__(self, other):
        
        if type(other) is float or type(other) is int:
            
            magnitude = self.magnitude + other
            
            if magnitude >= 0:
                angle = self.angle
            else:
                angle = self.angle + pi
                magnitude = -magnitude
            
            return Vector2D(angle = angle, magnitude = magnitude)
        
        elif type(other) is Vector2D:
            
            x = self.x + other.x
            y = self.y + other.y
            
            return Vector2D(x = x, y = y)
        
        
    def __sub__(self, other):
        
        if type(other) is float or type(other) is int:
            
            magnitude = self.magnitude - other
            
            if magnitude >= 0:
                angle = self.angle
            else:
                angle = self.angle + pi
                magnitude = -magnitude
            
            return Vector2D(angle = angle, magnitude = magnitude)
        
        elif type(other) is Vector2D:
            
            x = self.x - other.x
            y = self.y - other.y
            
            return Vector2D(x = x, y = y)
        
        
    def __mul__(self, other):
        
        if type(other) is float or type(other) is int:
            
            magnitude = self.magnitude * other
            
            if magnitude >= 0:
                angle = self.angle
            else:
                angle = self.angle + pi
                magnitude = -magnitude
            
            return Vector2D(angle = angle, magnitude = magnitude)
        
        elif type(other) is Vector2D:
            
            x = self.x * other.x
            y = self.y * other.y
            
            return Vector2D(x = x, y = y)
        
        
    def __truediv__(self, other):
        
        if type(other) is float or type(other) is int:
            
            magnitude = self.magnitude / other
            
            if magnitude >= 0:
                angle = self.angle
            else:
                angle = self.angle + pi
                magnitude = -magnitude
            
            return Vector2D(angle = angle, magnitude = magnitude)
        
        elif type(other) is Vector2D:
            
            if other.x != 0:
                x = self.x / other.x
            else:
                x = self.x
                
            if other.y != 0:
                y = self.y / other.y
            else:
                y = self.y
            
            return Vector2D(x = x, y = y)
            
            
    def setAngle(self, angle):
        self.angle = angle
        self.updXY()


    def setMagnitude(self, magnitude):
        self.magnitude = magnitude
        self.updXY()
        
    def setMA(self, angle, magnitude):
        self.angle = angle
        self.magnitude = magnitude
        self.updXY()


    def setX(self, x):
        self.x = x
        self.updMA()


    def setY(self, y):
        self.y = y
        self.updMA()
        
    def setXY(self, x, y):
        self.x = x
        self.y = y
        self.updMA()


    def updXY(self):
        self.x = cos(self.angle) * self.magnitude
        
        if (self.angle != 0) or (self.angle != pi):
            self.y = sin(self.angle) * self.magnitude
        else:
            self.y = 0.0
        
        
    def updMA(self):
        self.magnitude = sqrt(self.x ** 2 + self.y ** 2)
        
        if (self.x != 0) and (self.y != 0):
            if (self.x >= 0):
                self.angle = atan(self.y / self.x)
            else:
                self.angle = pi + atan(self.y / self.x)
        elif self.x == 0:
            if self.y > 0:
                self.angle = pi/2
            elif self.y < 0:
                self.angle = -pi/2
        elif self.y == 0:
            if self.x > 0:
                self.angle = 0.0
            elif self.x < 0:
                self.angle = pi
        
        if (self.x < 0):
            self.angle = self.angle + pi
    
    def split(self, split_angle):
        return Vector2D(angle = split_angle + self.angle, magnitude = self.magnitude)
    
        #temp_angle = self.angle
        #self.setAngle(split_angle + self.angle)
        #ret_list = [self.x, self.y]
        #self.setAngle(temp_angle)
        #return ret_list
            
            
        
            
            
