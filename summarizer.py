from helpers import sql, func


runID = input("Which run to summarise?: ") 
step = int(input("Boxplot generation step: "))

scores = []

gen_count = sql.autoread("gatar_runs", "run" + str(runID), select="generation")[-1] + 1

sdata = sql.autoread("gatar_runs", "run" + str(runID), select=["generation", "points"])

generations = [[] for _ in range(gen_count)]

for row in sdata:
    generations[row["generation"]].append(row["points"])
    

import matplotlib.pyplot as plt
import numpy as np

ticks = []
data = []

averages = []
best = []

for generation in generations:
    averages.append(sum(generation)/len(generation))
    best.append(max(generation))

for i in range(0, gen_count, step):
    ticks.append(i*step)
    data.append(generations[i])
    
    
plt.title("Average & Best")
plt.xlabel("Generation")
plt.ylabel("Score")
plt.plot(averages, label="Average")
plt.plot(best, label="Best")

xdata = np.array(range(gen_count))

plt.legend()

plt.savefig('average.png')
plt.figure()

plt.title("Score Summary")
plt.xlabel("Generation")
plt.ylabel("Score")
#plt.xticks(ticks, np.array(ticks)*step)
plt.boxplot(data, positions=range(0, gen_count, step), widths=(1/len(data))*len(generations)/2)

plt.savefig('boxplots.png')
plt.show()
