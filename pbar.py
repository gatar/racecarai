# progress bar for python

import sys
import os

class PBar:
    
    
    def __init__(self, *, width=None, label="   ", symbol="="):
        if width is None:
            rows, columns = os.popen('stty size', 'r').read().split()
            width = int(columns) - (4 + len(label)) 
        self.width = width
        self.label = label
        self.symbol = symbol
        self.count = 0 # amount of bars
        self.ended = False
        # setup toolbar
        sys.stdout.write(label + "[%s]" % (" " * width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (width+1)) # return to start of line, after '['


    def redraw(self):
        sys.stdout.write(self.label + "[%s]" % (" " * self.width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (self.width+1)) # return to start of line, after '['
        self.ended = False
        count = self.count
        self.count = 0
        self.set(count)


    def add(self):
        if self.count < self.width:
            self.count += 1
            sys.stdout.write(self.symbol)
            sys.stdout.flush()
        else:
            self.end()
       
       
    def rem(self):
        if self.count > 0:
            self.count -= 1
            sys.stdout.write("\b \b")
            sys.stdout.flush()
            
            
    def set(self, count):
        diff = int(count) - self.count
        
        if diff < 0:
            for _ in range(diff*-1):
                self.rem()
        elif diff > 0:
            for _ in range(diff):
                self.add()
                
    
    def percent(self, perc):
        self.set(self.width*perc)
            
    
    def end(self):
        if self.ended != True:
            self.ended = True
            sys.stdout.write("] \n") # this ends the progress bar 


