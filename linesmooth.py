import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.interpolate import interp1d

x = [-0.25, -0.625, -0.125, -1.25, -1.125, -1.25,
     0.875, 1.0, 1.0, 0.5, 1.0, 0.625, -0.25]
y = [1.25, 1.375, 1.5, 1.625, 1.75, 1.875, 1.875,
     1.75, 1.625, 1.5, 1.375, 1.25, 1.25]
     
#x = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
#y = [2000, 2200, 2700, 1800, 1000, 5000, 4900, 4800, 3500, 2000, 1800]

# Pad the x and y series so it "wraps around".
# Note that if x and y are numpy arrays, you'll need to
# use np.r_ or np.concatenate instead of addition!
orig_len = len(x)
x = x[-3:-1] + x + x[1:3]
y = y[-3:-1] + y + y[1:3]

print(x)

t = np.arange(len(x))

resolution = 0.1

while True:
    print("resolution", resolution)
    print("nodes", resolution * orig_len)
    ti = np.linspace(2, orig_len + 1, int(resolution * orig_len))

    xi = interp1d(t, x, kind='cubic')(ti)
    yi = interp1d(t, y, kind='cubic')(ti)
    
    

    a = []
    b = []

    for i in range(len(ti)):
        a = math.sqrt(xi[i]**2 + yi[i]**2)
        b = math.sqrt(xi[i]**2 + yi[i]**2)

    from helpers import trig

    i = 0
    sum = 0

    while i < len(xi) - 1:
        sum += trig.distance2D(xi[i], yi[i], xi[i+1], yi[i+1])
        i += 1

    print("AVG distance between points: " + str(sum/len(xi)))



    plt.plot(xi, yi, "r-")
    #fig, ax = plt.subplots()
    #ax.plot(xi, yi)
    #ax.plot(x, y)
    #ax.margins(0.05)
    plt.show()

    resolution += 0.1
