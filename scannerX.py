from helpers import intersect
from helpers.trig import distance2D
from math import sin, cos, pi, inf, sqrt


class NoNodesException(Exception):
    pass




class ScannerX:
    
    
    def __init__(self, *, angs, dist, car, track, debug=False): # angs list in radians, dist in metres
    
        self.angs = angs
        self.dist = dist
        self.car = car
        self.track = track
        self.debug = debug
        
    
    def scan(self):
        
        # get data
        x = self.car.x
        y = self.car.y
        angs = [x + self.car.car_angle.angle for x in self.angs]
        
        try:
            res = self.get_optimized(x, y)
        except IndexError as e:
            raise NoNodesException()
        
        outer = res[0]
        inner = res[1]
        nearest = res[2]
        
        res = [nearest[3]]
        
        for ang in angs:
            dists = []
            
            for series in inner:
                dists += self.get_distances(ang, x, y, series, "inner")
            for series in outer:
                dists += self.get_distances(ang, x, y, series, "outer")
            
            res.append(min(dists))
            
        if self.debug:
            import matplotlib.pyplot as plt
            
            for series in inner:
                ix = []
                iy = []
                for point in series:
                    ix.append(point[0])
                    iy.append(point[1])
                plt.plot(ix, iy)
            
            for series in outer:
                ox = []
                oy = []
                for point in series:
                    ox.append(point[0])
                    oy.append(point[1])
                plt.plot(ox, oy)
                
            for sect in res[1:]:
                plt.plot([x, sect[2]], [y, sect[3]])
                
                
            plt.plot(x, y, marker="*")
            plt.show()
                
        return res

#        
#        for ang in angs:
#            # Calculate k and m for line
#            k = sin(ang)/cos(ang) # k-värde
#            m = -k*x+y # m-värde
#            
#            # Calculate diff in x for desired "sensor dist (range)"
#            # for "rear facing" angles (180-90 and (-180)-(-90)) xdiff should be negative
#            # because cosine of those angles is negative no additoinal calculations have to be made
#            xdiff = cos(ang) * self.dist
#            
#            # Calculate line endpoints
#            xe = x + xdiff
#            ye = k*xe+m
#            
#            if self.debug:
#                plt.plot([x, xe], [y, ye])
#            
#            # make it a line
#            L1 = intersect.line([x, y], [xe, ye])
#            
#            # store all intersections in lists
#            outSects = []
#            inSects = []
#            
#            # Check intersections for outer track
#           for i in range(len(outer)-1):
#                # make a new line
#                L2 = intersect.line(outer[i], outer[i+1])
#                # check for interesection with L1
#                sect = intersect.intersection(L1, L2)
#                if sect:
#                    outSects.append(sect)
#                    if self.debug: 
#                        plt.plot(sect[0], sect[1], marker="*")
#            
#            # Check intersections for inner track
#            for i in range(len(inner)-1):
#                # make a new line
#                L2 = intersect.line(inner[i], inner[i+1])
#                # check for intersection with L1
#                sect = intersect.intersection(L1, L2)
#                if sect:
#                    inSects.append(sect)
#                    if self.debug:
#                        plt.plot(sect[0], sect[1], marker="*")
#                    
#                    
#            # Calculate distances for intersections
#            dists = []
#            
#            for sect in outSects:
#                dists.append([distance2D(sect[0], sect[1], x, y), "outer"])
#                
#            for sect in inSects:
#                dists.append([distance2D(sect[0], sect[1], x, y), "inner"])
#                
#            # if nothing in range detected
#            if len(dists) == 0:
#                dists.append([self.dist, "nothing in range"])
#        
#            # return closest, smallest value in first element, falls back to [1] if [0] #are the same
#            res.append(min(dists))
#            
#        if self.debug:
#            plt.show()
#            
#        # get distance from start
#        res.append(nearest[3])
#            
#        return res
    
    def get_distances(self, ang, x, y, series, side):
        
        # Calculate k and m for line
        k = sin(ang)/cos(ang) # k-värde
        m = -k*x+y # m-värde
            
        # Calculate diff in x for desired "sensor dist (range)"
        # for "rear facing" angles (180-90 and (-180)-(-90)) xdiff should be negative
        # because cosine of those angles is negative no additoinal calculations have to be made
        xdiff = cos(ang) * self.dist
            
        # Calculate line endpoints
        xe = x + xdiff
        ye = k*xe+m
        
        # make it a line
        L1 = intersect.line([x, y], [xe, ye])
        
        # store all intersections in list
        sects = []
        
        # Check intersections for track series
        for i in range(len(series)-1):
            # make a new line
            L2 = intersect.line(series[i], series[i+1])
            # check for interesection with L1
            sect = intersect.intersection(L1, L2)
            if sect:
                sects.append(sect)
                    
        # Calculate distances for intersections
        dists = []
        
        for sect in sects:
            dists.append([distance2D(sect[0], sect[1], x, y), side, sect[0], sect[1]])
            
        # if nothing in range detected
        if len(dists) == 0:
            dists.append([self.dist, "nothing", xe, ye])
    
        # return closest, smallest value in first element, falls back to [1] if [0] are the same
        return dists
        
        
        
    def get_optimized(self, x, y):
        ninner = get_nearby(self.track["inner"], x, y, self.dist * 1.0)
        nouter = get_nearby(self.track["outer"], x, y, self.dist * 1.0)
        
        nearest = []        
        inner = []
        for section in ninner:
            inner.append(simplify(section, 0.45, 1))
            nearest.append(get_nearest(section, x, y))
        
        outer = []
        for section in nouter:
            outer.append(simplify(section, 0.45, 1))
            
        #print(outer[0][0])
        
        return [outer, inner, min(nearest)[1]]
    
    
def get_nearest(series, x, y):
    
    mindist = inf
    
    for px in series:
        dist = sqrt(((x-px[0]) ** 2) + ((y-px[1]) ** 2))
        if dist < mindist:
            mindist = dist
            nearest = [dist, px]
            
    return nearest
        

def get_nearby(series, x, y, dist):
   
    nodes = []
    
    for node in series:
        if abs(node[0] - x) <= dist:
            if abs(node[1] - y) <= dist:
                nodes.append(node)
                
    # order nodes
    sections = [[]]
    
    prevID = nodes[0][2] - 1
    secID = 0
    
    for node in nodes:
        if node[2] == prevID + 1:
            prevID = node[2]
            sections[secID].append(node)
        else:
            secID += 1
            prevID = node[2]
            sections.append([])
            sections[secID].append(node)

            
    # add in reverse order
    
    #res = []
    
    #for i in range(len(sections)-1, 0-1, -1):
     #   for node in sections[i]:
      #      res.append(node)
                
    #return res
    
    return sections


def k(dx, dy): # calculate k-value, delta x and delta y
    if dx == 0:
        if dy < 0:
            return -(10 ** 16)
        elif dy > 0:
            return 10 ** 16
        elif dy == 0:
            raise Exception("WTH this is the same point")
    else:
        return dy/dx


def simplify(series, tolerance, kpass): # this code is confusing, plz fix
    
    # simplify by adjacency
    s1 = []
    
    s1.append(series[0])
    
    i = 1
    
    while i < len(series)-1:
        if (series[i][0] == series[i-1][0]): # x doesnt change, y does
            while (series[i][0] == series[i-1][0]):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][1] == series[i-1][1]): # y doesnt change, x does
            while (series[i][1] == series[i-1][1]):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]+1): # both x and y increase
            while (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]+1):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]-1): # both x and y decrease
            while (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]-1):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]-1): # x increase, y decrease
            while (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]-1):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]+1): # x decrease, y increase
            while (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]+1):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        else:
            s1.append(series[i])
            i += 1
            
    for _ in range(kpass):
            
        # simplify by k-value
        s2 = []
        prevk = 0
        i = 0
        
        while i < (len(s1)-1):
            if abs(k(s1[i][0]-s1[i+1][0], s1[i][1]-s1[i+1][1]) - prevk) <= abs(prevk * tolerance):
                i += 1
            else:
                s2.append(s1[i])
                prevk = k(s1[i][0]-s1[i+1][0], s1[i][1]-s1[i+1][1])
                i += 1
                
        s2.append(s1[i])
                
        s1 = s2
            
    return s1
            
