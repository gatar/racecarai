from helpers import intersect
from helpers.trig import distance2D
from math import sin, cos, sqrt, pi, inf


class Scanner:
    
    
    def __init__(self, *, angs, dist, car, track): # angs list in radians, dist in metres
        
        self.angs = angs
        self.dist = dist
        self.car = car
        self.track = track


    def scan(self):
        
        node_dist = self.dist * 1
        
        # parse data
        x = self.car.x
        y = self.car.y
        angs = [x + self.car.ang for x in self.angs]
        dist = self.dist
        track = self.track
        
        # get only track nodes that are close
        outer = []
        
        for node in track["outer"]:
            if abs(node[0] - x) <= node_dist:
                if abs(node[1] - y) <= node_dist:
                    outer.append(node)
                    
        inner = []
        
        for node in track["inner"]:
            if abs(node[0] - x) <= node_dist:
                if abs(node[1] - y) <= node_dist:
                    inner.append(node)
                    
        # simplify
        outer = self.simplify(outer, 0.45, 1)
        inner = self.simplify(inner, 0.45, 1)
        
        # wrap around degrees THIS SHOULD BE IN CAR CLASS INSTEAD
        #if ang > pi:
        #    ang = -(pi - ang % pi)
        #    
        #if ang < -pi:
        #    ang = (pi + ang % -pi)
        
        res = []
        
        for ang in angs:
            # Calculate k and m for line
            k = sin(ang)/cos(ang) # k-värde
            m = -k*x+y # m-värde
            
            # Calculate diff in x for desired "sensor dist (range)"
            # for "rear facing" angles (180-90 and (-180)-(-90)) xdiff should be negative
            # because cosine of those angles is negative no additoinal calculations have to be made
            xdiff = cos(ang) * dist
            
            # Calculate line endpoints
            xe = x + xdiff
            ye = k*xe+m
            
            # make it a line
            L1 = intersect.line([x, y], [xe, ye])
            
            # store all intersections in lists
            outSects = []
            inSects = []
            
            # Check intersections for outer track
            for i in range(len(outer)-1):
                # make a new line
                L2 = intersect.line(outer[i], outer[i+1])
                # check for interesection with L1
                sect = intersect.intersection(L1, L2)
                if sect:
                    outSects.append(sect)
            
            # Check intersections for inner track
            for i in range(len(inner)-1):
                # make a new line
                L2 = intersect.line(inner[i], inner[i+1])
                # check for intersection with L1
                sect = intersect.intersection(L1, L2)
                if sect:
                    inSects.append(sect)
                    
                    
            # Calculate distances for intersections
            dists = []
            
            for sect in outSects:
                dists.append([distance2D(sect[0], sect[1], x, y), "outer"])
                
            for sect in inSects:
                dists.append([distance2D(sect[0], sect[1], x, y), "inner"])
                
            # if nothing in range detected
            if len(dists) == 0:
                dists.append([dist, "lmao"])
        
            # return closest, smallest value in first element, falls back to [1] if [0] are the same
            res.append(min(dists))
            
        return res
    
    def simplify(self, series, tolerance, kpass):
        
        # simplify by adjacency
        s1 = []
        
        i = 1
        
        while i < len(series)-1:
            if (series[i][0] == series[i-1][0]): # x doesnt change, y does
                while (series[i][0] == series[i-1][0]):
                    if i == (len(series)-1):
                        break
                    i += 1
                s1.append(series[i])
                i += 1
            elif (series[i][1] == series[i-1][1]): # y doesnt change, x does
                while (series[i][1] == series[i-1][1]):
                    if i == (len(series)-1):
                        break
                    i += 1
                s1.append(series[i])
                i += 1
            elif (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]+1): # both x and y increase
                while (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]+1):
                    if i == (len(series)-1):
                        break
                    i += 1
                s1.append(series[i])
                i += 1
            elif (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]-1): # both x and y decrease
                while (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]-1):
                    if i == (len(series)-1):
                        break
                    i += 1
                s1.append(series[i])
                i += 1
            elif (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]-1): # x increase, y decrease
                while (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]-1):
                    if i == (len(series)-1):
                        break
                    i += 1
                s1.append(series[i])
                i += 1
            elif (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]+1): # x decrease, y increase
                while (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]+1):
                    if i == (len(series)-1):
                        break
                    i += 1
                s1.append(series[i])
                i += 1
            else:
                s1.append(series[i])
                i += 1
                
        for _ in range(kpass):
                
            # simplify by k-value, first pass
            s2 = []
            prevk = 0
            i = 0
            
            while i < (len(s1)-1):
                if abs(k(s1[i][0]-s1[i+1][0], s1[i][1]-s1[i+1][1]) - prevk) <= abs(prevk * tolerance):
                    i += 1
                else:
                    s2.append(s1[i])
                    prevk = k(s1[i][0]-s1[i+1][0], s1[i][1]-s1[i+1][1])
                    i += 1
                    
            s2.append(s1[i])
                    
            s1 = s2
                
        return s1
    
    
def k(dx, dy): # calculate k-value, delta x and delta y
    if dx == 0:
        if dy < 0:
            return -(10 ** 16)
        elif dy > 0:
            return 10 ** 16
        elif dy == 0:
            raise Exception("WTH this is the same point")
    else:
        return dy/dx
