from PIL import Image
from helpers import fs, img
import numpy
import matplotlib.pyplot as plt
from math import sqrt, inf
from collections import deque


def k(dx, dy): # calculate k-value, delta x and delta y
    if dx == 0:
        if dy < 0:
            return -(10 ** 16)
        elif dy > 0:
            return 10 ** 16
        elif dy == 0:
            raise Exception("WTH this is the same point")
    else:
        return dy/dx


def simplify(series, tolerance, kpass):
    
    # simplify by adjacency
    s1 = []
    
    i = 0
    
    while i < len(series)-1:
        if (series[i][0] == series[i-1][0]): # x doesnt change, y does
            while (series[i][0] == series[i-1][0]):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][1] == series[i-1][1]): # y doesnt change, x does
            while (series[i][1] == series[i-1][1]):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]+1): # both x and y increase
            while (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]+1):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]-1): # both x and y decrease
            while (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]-1):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]-1): # x increase, y decrease
            while (series[i][0] == series[i-1][0]+1) and (series[i][1] == series[i-1][1]-1):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        elif (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]+1): # x decrease, y increase
            while (series[i][0] == series[i-1][0]-1) and (series[i][1] == series[i-1][1]+1):
                if i == (len(series)-1):
                    break
                i += 1
            s1.append(series[i])
            i += 1
        else:
            s1.append(series[i])
            i += 1
            
    for _ in range(kpass):
            
        # simplify by k-value, first pass
        s2 = []
        prevk = 0
        i = -1
        
        while i < (len(s1)-1):
            if abs(k(s1[i][0]-s1[i+1][0], s1[i][1]-s1[i+1][1]) - prevk) <= abs(prevk * tolerance):
                i += 1
            else:
                s2.append(s1[i])
                prevk = k(s1[i][0]-s1[i+1][0], s1[i][1]-s1[i+1][1])
                i += 1
            
    return s2
    

tpath = "tracks/"

tname = input("Input track filename (with .png etc): ")
tfull = tpath + tname

if not fs.exists(tfull):
    raise Exception("Invalid track filename")
else:
    print("Reading track img: " + tfull)
    image = Image.open(tfull, "r")
    imagedata = img.read(image)
    image.close()
    
    # for key, value in imagedata.items():
    #    trackdata[key] = value
    
    name = input("What is the name of the track?: ")
    scale = float(input("What is the scale of the track (metres/pixels)?: ")) # 1 pixel on track = how many metres?
    start = [0, 0] # start co-ords
    heading = [0, 0] # pixel to move towards from start
    outer = [] # outer edge of track
    inner = [] # inner edge of track
    
    print("Stripping unnecessary pixels...")
                                       
    # strip unnecessary pixels
    for x in range(len(imagedata["px"])):
            for y in range(len(imagedata["px"][0])):
                px = imagedata["px"][x][y] # get pixel
                
                if len(px) == 3: # if pixel has no opacity value pretend it is completely opaque
                    px.append(255)
                    
                #if 0 < px[3] < 255:
                #    raise Exception("Non-complete erasure detected, risk for errornous track reading")
                    
                if px == [0, 255, 0, 255]: # green, start point
                    start = [x, y]
                elif px == [255, 255, 0, 255]: # yellow, heading
                    heading = [x, y]
                elif px == [255, 0, 0, 255]: # red, outer edge of track
                    outer.append([x, y])
                elif px == [0, 0, 255, 255]: # blue, inner edge of track
                    inner.append([x, y])
    

    
    print("Serializing track in correct order...")
    
    # serialize outer track in correct order
    print("outer...")
    
    souter = [outer[0]] # serialized outer pixels
    cur = outer[0] # current pixel
    
    for _ in range(len(outer)):
        for px in outer:
            if ((px[0] == cur[0]+1) or (px[0] == cur[0]-1) or (px[0] == cur[0])) and ((px[1] == cur[1]+1) or (px[1] == cur[1]-1)  or (px[1] == cur[1])) and (px not in souter):
                souter.append(px)
                cur = px
                break

    # serialize inner track in correct order
    print("inner...")
    
    sinner = [inner[0]] # serialized inner pixels
    cur = inner[0] # current pixel
        
    for _ in range(len(inner)):
        for px in inner:
            if ((px[0] == cur[0]+1) or (px[0] == cur[0]-1) or (px[0] == cur[0])) and ((px[1] == cur[1]+1) or (px[1] == cur[1]-1)  or (px[1] == cur[1])) and (px not in sinner):
                sinner.append(px)
                cur = px
                break
            
    print("DONE")
    
    # scale track from pixels to metres
    print("Scaling track from pixels to metres...")
    
    start[0] *= scale
    start[1] *= scale
    
    heading[0] *= scale
    heading[1] *= scale
    
    for px in souter:
        px[0] *= scale
        px[1] *= scale
        
    for px in sinner:
        px[0] *= scale
        px[1] *= scale
        
    # rotate track
    print("Rotating track")
    
    # give ids to nodes
    ID = 0
    
    for px in sinner:
        px.append(ID)
        ID += 1
        
    ID = 0
    
    for px in souter:
        px.append(ID)
        ID += 1
    
    # get nearest node to start
    closest = inf
    ci = 0
    
    for i in range(len(sinner)):
        px = sinner[i]
        dist = sqrt(((start[0]-px[0]) ** 2) + ((start[1]-px[1]) ** 2))
        if dist < closest:
            closest = dist
            ci = i
    
    print("CLOSEST")
    print(closest)
    print(ci)
    print(sinner[ci])
    
    deq = deque(sinner)
    deq.rotate(-ci)
    
    sinner = list(deq)
    
    print("DONE")
        
    # calculate distances
    print("Calulating distances")
    distance = 0
    prev = sinner[0]
    for px in sinner:
        distance += sqrt(((prev[0]-px[0]) ** 2) + ((prev[1]-px[1]) ** 2))
        px.append(distance)
        prev = px

    # visualisation
    print("Visualising track...")

    # setup variables for visualisation
    outerXs = []
    outerYs = []
    
    innerXs = []
    innerYs = []
    
    for px in souter:
        outerXs.append(px[0])
        outerYs.append(px[1])
        
    outerXs.append(souter[0][0])
    outerYs.append(souter[0][1])
        
    for px in sinner:
        innerXs.append(px[0])
        innerYs.append(px[1])
        
    innerXs.append(sinner[0][0])
    innerYs.append(sinner[0][1])
    
    plt.plot(outerXs, outerYs, marker="o")
    plt.plot(innerXs, innerYs, marker="o")
    plt.plot(start[0], start[1], marker="*")
    plt.plot(heading[0], heading[1], marker="*")
    
    print("Inspect track and close popup when done")
    plt.show()
    
    if input("Is the track OK (y)?: ") == "y":
    
        # save track
        trackdata = {
            "name": name,
            "scale": scale,
            "start": start,
            "heading": heading,
            "outer": souter,
            "inner": sinner
            }

        print("Saving track json: " + tpath + tname.split(".")[0] + ".json")
        fs.saveJson(tpath + tname.split(".")[0] + ".json", trackdata)
        
    else:
        print("Discarding track...")
