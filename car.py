from helpers.trig import componentBrk
from math import sqrt, pi
from numpy import clip
from carPhysics import model1 as fsx


class Car:
    
    
    def __init__(self, x, y, ang, driver):
        
        # car position and speed
        self.width = 0.7
        self.x = x
        self.y = y
        self.truev = 0
        self.ang = ang
        self.tickSpeed = 0.01
        self.v = 0
        self.steering = 0 # current steering in radians/second
        self.physics = fsx(self)
        
        # car inputs
        self.throttle = 0.0
        self.brake = 0.0
        self.steeringTarget = 0.0
        
        # car specs
        self.force = 10000 # Newton force at wheels # OLD:150000 # joules/second, derived from horsepower
        self.bForce = -20000 # Newton braking force at wheels
        self.m = 1500 # mass
        self.steeringChg = pi/10 # steering rate of change max in radians/second
        self.steeringMax = pi/6 # maximum steering angle in radians, also car angle max rate of change
        
    def steer(self, inputs):
        steeringwheel = inputs[0]
        gas = inputs[1]
        brake = inputs[2]
        self.throttle = gas
        #print("gas = " + str(gas))
        #print("throttle = " + str(self.throttle))
        self.brake = brake
        self.steeringTarget = steeringwheel- 0.5
        self.move()
        
    def updPos(self): # update position
        #print("Position updated")
        
        vx, vy = componentBrk(self.v, self.ang)
        
        self.x += vx * self.tickSpeed
        self.y += vy * self.tickSpeed
        
        #print(self.v, self.physics)
    
    
    def updAng(self):
        #print("Angle updated")
        
        self.steering += clip(self.steeringTarget - self.steering, -self.steeringChg*self.tickSpeed, self.steeringChg*self.tickSpeed)
        
        self.steering = clip(self.steering, -self.steeringMax, self.steeringMax)
        
        self.ang += self.steering * self.tickSpeed * self.v
        
        
    def updSpd(self): # update speed
        
        # a = F/m
        
        # dv = a*t = (F/m)*t
        self.v += (self.force/self.m)*self.throttle*self.tickSpeed
        self.v += (self.bForce/self.m)*self.brake*self.tickSpeed
        
        if self.v < 0:
            self.v = 0
        
        
    
    def move(self):
        for _ in range(100):
            self.updAng()
            self.updSpd()
            self.updPos()
        #print("Speed: " + str(self.v) + " X: " + str(self.x) + " Y: " + str(self.y) + " Angle: " + str(self.ang) + " Distance travelled: " + str((((self.x-self.oldx)**2)+((self.y-self.oldy)**2))**0.5) + " Tickspeed: " + str(self.tickSpeed))
        #print("Tickspeed: " + str(self.tickSpeed))
        

        
        
