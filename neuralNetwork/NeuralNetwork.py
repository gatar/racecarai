import numpy as np
import random
from tensorflow import keras, device, constant
#Axel

class NeuralNetwork:
    def __init__ (self, b, c, d, e, f, *, model=None): # klar och testad
        self.n = 4 #Mängden lager exklusive input
        self.input_nodes = b
        self.hidden_nodes1 = c
        self.hidden_nodes2 = d
        self.hidden_nodes3 = e
        self.output_nodes = f
        if isinstance(model, keras.Sequential):
            self.model = model
        else: # 
            self.model = self.createModel()
    
    def copy(self): # klar och testad. Denna funktion kallas via NamnPåNyaNätverket = NamnPåNätverketDuVillKopiera.copy() och den skapar ett nytt nätverket med samma egenskaper som nätverket du vill kopiera
        modelCopy = self.createModel()
        for i in range(int(self.n)):
            weights = self.model.layers[i].get_weights()
            modelCopy.layers[i].set_weights(weights)
        return NeuralNetwork(
            self.input_nodes, 
            self.hidden_nodes1,
            self.hidden_nodes2,
            self.hidden_nodes3,
            self.output_nodes,
            model = modelCopy
        )
    
    def mutate(self, rate): # klart och testat. Denna funktion går igenom alla weights i neurala nätverket och ändrar en viss andel (rate) av dem slumpmässigt.
        for i in range(self.n):
            weights = self.model.layers[i].get_weights().copy()
            mutatedWeights = weights
            for j in range(len(weights)):
                valueArr = weights[j]
                for b in range(len(valueArr)):
                    values = valueArr[b]
                    if type(values) is np.ndarray: 
                        for t in range(len(values)):
                            w = values[t]
                            if random.random()<rate:
                                values[t] = w + (random.gauss(0, 0.1))
            self.model.layers[i].set_weights(mutatedWeights)
        
    def predict(self, inputs): # klart och testat. Denna funktion ger nätverkets svar på inputs.
        #with device('/CPU:0'):
        #with device('/device:GPU:0'):
        return self.model(constant(inputs, dtype = "float32", shape = (1, self.input_nodes)), training = False)[0]
        
    
    def createModel(self): # klart och testat. Denna funktion skapar en ny model och lägger till modellens lager
        model = keras.Sequential()
        
        hidden1 = keras.layers.Dense(
            self.hidden_nodes1, 
            input_shape=[self.input_nodes],
            activation='sigmoid'
        )
        model.add(hidden1)
        hidden2 = keras.layers.Dense(
            self.hidden_nodes2,
            input_shape=[self.hidden_nodes2]
        )
        model.add(hidden2)
        hidden3 = keras.layers.Dense(
            self.hidden_nodes3,
            input_shape=[self.hidden_nodes3],
            activation='sigmoid'
        )
        model.add(hidden3)
        output = keras.layers.Dense(
            self.output_nodes,
            activation='hard_sigmoid'
        )
        model.add(output)
        return model
