from car_stuff.Wheel import Grippy19
from car_stuff.Physics import Physics
from Vector import Vector2D as Vector
test = Physics()
test.dt = 1
test.mass = 2000
test.v = Vector(angle = 2, magnitude = 20)
test.car_angle = Vector(angle = 2.2, magnitude = 1)
test.x = 0
test.y = 0
test.fl_wheel = Grippy19()
test.fr_wheel = Grippy19()
test.rl_wheel = Grippy19()
test.rr_wheel = Grippy19()
print("X: " + str(test.x) + " Y: " + str(test.y) + " car_angle: " + str(test.car_angle.angle) + " V.angle: " + str(test.v.angle) + " V.magnitude: " + str(test.v.magnitude))
for _ in range(10):
    test.fsx_update()
    print("X: " + str(test.x) + " Y: " + str(test.y) + " car_angle: " + str(test.car_angle.angle) + " V.angle: " + str(test.v.angle) + " V.magnitude: " + str(test.v.magnitude))