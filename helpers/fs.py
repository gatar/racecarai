import json
from os import path


# exception classes
class InvalidPath(Exception):
    pass


def exists(item):
    return path.exists(item)


def loadJson(path):
    if not exists(path):
        raise InvalidPath("The path does not exist")

    file = open(path, "r")
    try:
        dataJson = file.read()
    finally:
        file.close()

    data = json.loads(dataJson)

    return data


def saveJson(path, data):
    dataJson = json.dumps(data)

    file = open(path, "w")
    try:
        file.write(dataJson)
    finally:
        file.close()


def appendJson(path, data):

    if exists(path): 
        existing = loadJson(path)
    else:
        existing = []

    if type(existing) is not list: existing = [existing]
    existing.append(data)

    saveJson(path, existing)