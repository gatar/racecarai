# SOURCE: https://stackoverflow.com/a/20679579

from __future__ import division 

def line(p1, p2): # p1 [x, y], p2 [x, y]
    delta_y = (p1[1] - p2[1])
    delta_x = (p2[0] - p1[0])
    what_dis_is = (p1[0]*p2[1] - p2[0]*p1[1])
    return delta_y, delta_x, -what_dis_is, p1, p2

def intersection(L1, L2):
    # step 1: check if any point of line interesects
    D  = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]

    if D != 0:
        x = Dx / D
        y = Dy / D

        # step 2: parse line data into variables to make it easier to understand
        l1p1x = L1[3][0] # line 1, point 1, x co-ord
        l1p1y = L1[3][1]
        l1p2x = L1[4][0]
        l1p2y = L1[4][1]
        
        l2p1x = L2[3][0]
        l2p1y = L2[3][1]
        l2p2x = L2[4][0]
        l2p2y = L2[4][1]
        
        # step 3a: for some reason the boolean logic in step 3b will not pass if delta x or y for any of the lines is 0
        # mitigation: add a tiny value to the line coordinate, since the intersection point is already calculated
        # it will not affect the accuaracy of the result. Worst case scenario it will pass an invalid intersection point,
        # though it should not be far off from the desired one.
            
        crr = 10**-2 # correction to increase tolerance of boolean logic block below, maybe increase if some intersections are still skipped
        
        # step 3b: check if intersection point is on the line, thus checking if line SEGMENTS intersect and not entire lines
        if ((l1p1x - crr) <= x <= (l1p2x + crr)) or ((l1p2x - crr) <= x <= (l1p1x + crr)):
            if ((l1p1y - crr) <= y <= (l1p2y + crr)) or ((l1p2y - crr) <= y <= (l1p1y + crr)):
                if ((l2p1x - crr) <= x <= (l2p2x + crr)) or ((l2p2x - crr) <= x <= (l2p1x + crr)):
                    if ((l2p1y - crr) <= y <= (l2p2y + crr)) or ((l2p2y - crr) <= y <= (l2p1y + crr)):
                        return x, y
        
    else:
        return False
