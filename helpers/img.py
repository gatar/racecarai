from PIL import Image
from helpers import fs
import numpy


class UnsupportedMode(Exception):
	pass


def read(img):
    # get data
    px = list(img.getdata()) # make a list of image pixels
    w, h = img.size # get width and height
    mode = img.mode # get type of px values (like RGB)

    # parse mode into channel count
    if mode == "RGB":
        channels = 3
    elif mode == "RGBA":
    	channels = 4
    elif mode == "L":
        channels = 1
    else:
        raise UnsupportedMode("Mode: " + str(mode))

    # reshape px list into 2D array
    px = numpy.array(px).reshape((h, w, channels)).tolist()
    
    # read by Pillow as y, x (not x, y) thus the following code will flip it for convienience
    outer = []
    for x in range(len(px[0])): #len(imagedata["px"])
        inner = []
        for y in range(len(px)): #len(imagedata["px"][0])
            inner.append(px[y][x])
        outer.append(inner)
            
            

    return {"px": outer, "h": h, "w": w, "mode": mode}
