import mysql.connector
from decimal import Decimal


def connect(database=None):
    from helpers import fs
    cred = fs.loadJson("configs/sql.json")

    if database is None:
        mydb = mysql.connector.connect(
            host=cred["host"],
            user=cred["user"],
            password=cred["password"]
        )

    else:
        mydb = mysql.connector.connect(
            host=cred["host"],
            user=cred["user"],
            password=cred["password"],
            database=database
        )

    return mydb, mydb.cursor(buffered=True)


def rowcount(database, table, *, extra=None):
    db, dbCursor = connect(database)
    
    sql = "SELECT COUNT(*) FROM " + table
    
    if extra:
        sql += " " + extra

    dbCursor.execute(sql)
    rows = dbCursor.fetchone()[0]

    dbCursor.close()
    db.close()

    return rows


def lastrow(database, table):
    db, dbCursor = connect(database)

    dbCursor.execute("SELECT id FROM " + table + " ORDER BY id DESC LIMIT 1")

    data = dbCursor.fetchone()

    if data is None:
        last = 0
    else:
        last = data[0]

    dbCursor.close()
    db.close()

    return last


def simpleinsert(dbCursor, table, data):
    if type(data) is not list: data = [data]  # if data not list make it a list
    dict = data[0]  # get first dict in list to use as a pattern for SQL
    keylist = list(dict)  # store keys of dict in a specific order to not mix things up later
    
    # check if all dictoinary keys are the same
    for dic in data:
        if list(dic) != keylist:
            raise Exception("Not all dictionaries have same keys! Can not assume SQL pattern.")

    sql = "INSERT INTO " + table + " (" + str(keylist[0])  # insert first key in keylist

    for i in range(len(keylist) - 1): sql += "," + str(
        keylist[i + 1])  # for each key after the first insert "," and key

    sql += ") VALUES (%s" + ",%s" * (len(keylist) - 1) + ")"  # append "%s" for value insertion based on amount of keys

    def parse(keylist, dict):  # parse dict into tuple by a specific order dictated by keylist
        tupl = ()
        for key in keylist:
            value = dict[key]
            if type(value) is float: value = Decimal(value)
            tupl += (value,)
        return tupl

    if type(data) == list:  # if list of dicts then create list of val tuples
        val = []
        for dict in data: val.append(parse(keylist, dict))
        dbCursor.executemany(sql, val)
    else:  # if single dict create a single val tuple
        val = parse(keylist, dict)
        dbCursor.execute(sql, val)


def multinsert(database, table, datalist):
    db, dbCursor = connect(database)
    
    for data in datalist:
        simpleinsert(dbCursor, table, data)
    
    db.commit()
    dbCursor.close()
    db.close()
    

def autoinsert(database, table, data):
    db, dbCursor = connect(database)

    if type(data) is not list: data = [data]  # if data not list make it a list
    dict = data[0]  # get first dict in list to use as a pattern for SQL
    keylist = list(dict)  # store keys of dict in a specific order to not mix things up later
    
    # check if all dictoinary keys are the same
    for dic in data:
        if list(dic) != keylist:
            raise Exception("Not all dictionaries have same keys! Can not assume SQL pattern.")

    sql = "INSERT INTO " + table + " (" + str(keylist[0])  # insert first key in keylist

    for i in range(len(keylist) - 1): sql += "," + str(
        keylist[i + 1])  # for each key after the first insert "," and key

    sql += ") VALUES (%s" + ",%s" * (len(keylist) - 1) + ")"  # append "%s" for value insertion based on amount of keys

    def parse(keylist, dict):  # parse dict into tuple by a specific order dictated by keylist
        tupl = ()
        for key in keylist:
            value = dict[key]
            if type(value) is float: value = Decimal(value)
            tupl += (value,)
        return tupl

    if type(data) == list:  # if list of dicts then create list of val tuples
        val = []
        for dict in data: val.append(parse(keylist, dict))
        dbCursor.executemany(sql, val)
    else:  # if single dict create a single val tuple
        val = parse(keylist, dict)
        dbCursor.execute(sql, val)

    db.commit()
    dbCursor.close()
    db.close()


def autodelete(database, table, condition):
    db, dbCursor = connect(database)

    dbCursor.execute("DELETE FROM " + table + " " + condition)

    db.commit()
    dbCursor.close()
    db.close()
    

def autoupdate(database, table, dict, *, extra=None):
    db, dbCursor = connect(database)

    # dbCursor.execute("UPDATE table_name SET field1=%s ... field10=%s WHERE id=%s", (var1,... var10, id))

    keylist = list(dict)  # store keys of dict in a specific order to not mix things up later

    sql = "UPDATE " + table + " SET " + str(keylist[0]) + "=%s"  # insert first key in keylist

    for i in range(len(keylist) - 1): sql += "," + str(
        keylist[i + 1]) + "=%s"  # for each key after the first insert "," and key

    sql += " " + extra

    def parse(keylist, dict):  # parse dict into tuple by a specific order dictated by keylist
        tupl = ()
        for key in keylist:
            value = dict[key]
            if type(value) is float: value = Decimal(value)
            tupl += (value,)
        return tupl

    val = parse(keylist, dict)
    dbCursor.execute(sql, val)

    db.commit()
    dbCursor.close()
    db.close()


def autoread(database, table, *, select=None, extra=None, by=None, sort="ASC", limit=None):  # args after * are keyword only

    db, dbCursor = connect(database)

    if select is None:
        dbCursor.execute("SELECT * FROM " + table)  # get all column names
        keylist = [i[0] for i in dbCursor.description]  # store in list to preserve order in future operations
    else:
        keylist = []
        if type(select) is not list: select = [select]
        for item in select: keylist.append(item)

    sql = "SELECT " + str(keylist[0])  # append first key

    for i in range(len(keylist) - 1): sql += "," + str(
        keylist[i + 1])  # for each key after the first insert "," and key

    sql += " FROM " + table

    if extra is not None: sql += " " + extra  # extra section that can contain things like WHERE and GROUP BY

    if by is not None:
        sql += " ORDER BY " + by + " " + sort

    if limit is not None:  # check if limit value is an integer
        if type(limit) is int:
            sql += " LIMIT " + str(limit)
        else:
            raise Exception("LIMIT must be an integer")

    dbCursor.execute(sql)

    datalist = dbCursor.fetchall()

    data = []

    if select is not None and len(select) == 1:
        for item in datalist:
            data.append(item[0])
    else:
        for item in datalist:  # parse returned array from sql dbCursor by keylist to make a list of dictionaries
            dict = {}
            for i in range(len(keylist)):
                value = item[i]
                if type(value) is Decimal: value = float(value)
                dict[keylist[i]] = value
            data.append(dict)

    dbCursor.close()
    db.close()

    return data
