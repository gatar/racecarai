from math import sqrt, sin, cos


def distance2D(x1, y1, x2, y2):  # check distance on a 2d plane
    return sqrt(((x1 - x2) ** 2) + ((y1 - y2) ** 2))


def distance3D(x1, y1, z1, x2, y2, z2):  # check distance in 3d space
    return sqrt(((distance2D(x1, y1, x2, y2)) ** 2) + ((z1 - z2) ** 2))


def componentBrk(length, angle): # component breakdown (komposantuppdelning)
    
    #NOTE: angle in radians
    
    x = cos(angle) * length
    y = sin(angle) * length
    
    return x, y
