from scipy.optimize import curve_fit as fit
import numpy as np

"""
...fit functions require numpy arrays to work correctly. Like: np.array(python_array); Ex: np.array([1.0, 2.0, 3.0])

...func calculate the y of a function for a given x

polyfunc(x, *args) can calculate sum of n-polynomial functions, just add enough coefficients (a, b, c, d ...) where the last one is the intercept
"""


def linfunc(x, a, b):  # linear function y = ax + b
    return a * x + b


def linfit(xdata, ydata):
    popt, pcov = fit(linfunc, xdata, ydata)
    return popt, pcov


def powfunc(x, a, b):  # power function y = ax^b
    return a * np.power(x, b)


def powfit(xdata, ydata):
    popt, pcov = fit(powfunc, xdata, ydata)
    return popt, pcov


def polyfunc(x, *args):  # polynomial function y = ax^3 ... cx(^1) + d(x^0)
    sum = 0
    for power, coeff in enumerate(args[::-1]):  # iterate reverse of list, this way index (power) 0 is the last value which is the intercept and everything checks out. Same order as what is generated bu curve_fit.
        sum += coeff * np.power(x, power)  # single coeff*x^power "module"
    return sum


def polyfit(grade, xdata, ydata):
    # because of the way scipy.optimize.curve_fit() works it needs to know how many positional arguments there are
    # so they have to be went through, like a middle man.

    def poly1(x, a, b):
        return linfunc(x, a, b)

    def poly2(x, a, b, c):
        return polyfunc(x, a, b, c)

    def poly3(x, a, b, c, d):
        return polyfunc(x, a, b, c, d)

    def poly4(x, a, b, c, d, e):
        return polyfunc(x, a, b, c, d, e)

    def poly5(x, a, b, c, d, e, f):
        return polyfunc(x, a, b, c, d, e, f)

    def poly6(x, a, b, c, d, e, f, g):
        return polyfunc(x, a, b, c, d, e, f, g)

    def poly7(x, a, b, c, d, e, f, g, h):
        return polyfunc(x, a, b, c, d, e, f, g, h)

    if 1 <= grade <= 7:
        function = locals()["poly" + str(grade)]
    else:
        raise Exception("Only grades 1-7 supported")

    popt, pcov = fit(function, xdata, ydata)  # curve_fit itself; popt is a numpy array of the calculated coefficients and intercept; pcov is a thing
    return popt, pcov


# print(polyfit(1, np.array([5, 30, 80, 150, 200, 201, 202, 203, 204, 205]), np.array([1, 0.56, 0.55, 0.54, 0.53, 0.52, 0.51, 0.50, 0.49, 0.48])))
# print(linfit(np.array([5, 30, 80, 150, 200, 201, 202, 203, 204, 205]), np.array([1, 0.56, 0.55, 0.54, 0.53, 0.52, 0.51, 0.50, 0.49, 0.48])))
# print(powfit(np.array([5, 30, 80, 150, 200, 201, 202, 203, 204, 205]), np.array([1, 0.56, 0.55, 0.54, 0.53, 0.52, 0.51, 0.50, 0.49, 0.48])))

# popt, pcov = polyfit(5, np.array([5, 30, 80, 150, 200, 201, 202, 203, 204, 205]), np.array([1, 0.56, 0.55, 0.54, 0.53, 0.52, 0.51, 0.50, 0.49, 0.48]))

# print(polyfunc(121.054, *popt))
