from scannerX import ScannerX
from helpers import fs
from car_stuff.Car import Large_Sedan
from math import pi

# read track
track = fs.loadJson("tracks/mantorp.json")

# create car and set its position to tracks start position
car = Large_Sedan()
car.x = track["start"][0]
car.y = track["start"][1]

# max distance to scan at
dist = 50

# set angles to scan at
angs = [
    pi/2+0.000001, # ang0
    pi/2+1.000001, # ang1
    pi/2+2.000001, # ang2
    pi/2+3.000001, # ang3
    pi/2+4.000001  # ang4
    ]

scanner = ScannerX(angs = angs, dist = dist, car = car, track = track)

try:
    res = scanner.scan()
except NoNodesException:
    pass

print("Distance from start: " + str(res[0]))

# loop through index 1-END (ie exclude index 0)
i = 0
for dist in res[1:]:
    print("ang" + str(i) + " - " + dist[1].title() + " wall is " + str(dist[0]) + " metres away from car")
    i += 1 # this is not essential
